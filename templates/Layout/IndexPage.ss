<header class="header-area transparent-bar section-padding-1">
    <div class="header-bottom background-rgb-1">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-xl-2 col-lg-2">
                    <div class="logo logo-width">
                        <a href="/"><img src="$SiteSettings.Logo.ScaleWidth(150).AbsoluteLink" alt="$SiteSettings.Title logo"></a>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8 d-flex justify-content-center position-static">
                    <div class="main-menu menu-lh-1 main-menu-white main-menu-padding-1 menu-fw-400">
                        <nav>
                            <ul>
                                <% loop $SiteSettings.MainMenu.MenuItems %>
                                    <% if $Children.count %>
                                        <li>
                                            <a>$Title <i class="fa fa-angle-down"></i></a>
                                            <ul class="sub-menu-width">
                                                <% loop $Children %>
                                                    <li><a href="$Link">$Title</a></li>
                                                <% end_loop %>
                                            </ul>
                                        </li>
                                    <% else %>
                                        <li><a <% if isCurrent %>class="active"<% end_if %> href="$Link">$Title</a></li>
                                    <% end_if %>
                                <% end_loop %>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2">
                    <div class="header-right-wrap header-right-flex">
                        <div class="same-style same-style-white header-wishlist">
                            <a href="$WishList.AbsoluteLink()"><i class="fa fa-heart-o"></i></a>
                        </div>
                        <div class="same-style same-style-white cart-wrap">
                            <% if $Cart.getItemCount(0) %>
                                <a href="$Cart.AbsoluteLink()" class="cart-active">
                                    <i class="dlicon shopping_bag-20"></i>
                                    <span class="count-style">$Cart.getItemCount(0)</span>
                                </a>
                            <% else %>
                                <a href="$Cart.AbsoluteLink()">
                                    <i class="dlicon shopping_bag-20"></i>
                                </a>
                            <% end_if %>
                        </div>
                        <div class="same-style same-style-white header-search">
                            <a class="search-active" href="/search">
                                <i class="dlicon ui-1_zoom"></i>
                            </a>
                        </div>
                        <div class="same-style same-style-white">
                            <a href="$Customers.AbsoluteLink('profile')">
                                <i class="dlicon users_circle-10"></i>
                            </a>
                        </div>
                        <div class="same-style same-style-white header-off-canvas">
                            <a class="header-aside-button" href="#">
                                <i class="dlicon ui-3_menu-left"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<% include HeaderAssists %>

<% if $Theme.CustomField('HomeFeaturedProduct1').Value() || $Theme.CustomField('HomeFeaturedProduct2').Value() || $Theme.CustomField('HomeFeaturedProduct3').Value() %>
<div class="slider-area">
    <div class="container-fluid p-0">
        <div class="main-slider-active owl-carousel slider-dot-position-2 slider-dot-style-1 slider-dot-hm2 slider-img-width">
            <% if $Theme.CustomField('HomeFeaturedProduct1').Value() %>
                <% include HomeBannerSlide Product=$Theme.CustomField('HomeFeaturedProduct1').Value(),Pos=1 %>
            <% end_if %>
            <% if $Theme.CustomField('HomeFeaturedProduct2').Value() %>
                <% include HomeBannerSlide Product=$Theme.CustomField('HomeFeaturedProduct2').Value(),Pos=2 %>
            <% end_if %>
            <% if $Theme.CustomField('HomeFeaturedProduct3').Value() %>
                <% include HomeBannerSlide Product=$Theme.CustomField('HomeFeaturedProduct3').Value(),Pos=3 %>
            <% end_if %>
        </div>
    </div>
</div>
<% end_if %>

<% if ObjectsByTag('Collection', 'show_in_home').count %>
    <div class="categories-area pt-130 pb-120 section-padding-1">
        <div class="container-fluid">
            <div class="section-title-2 mb-70 text-center">
                <h2>$Theme.CustomField('HomeCategoriesTitle')</h2>
                <p>$Theme.CustomField('HomeCategoriesSubTitle')</p>
            </div>
            <div class="categories-slider-active dot-style-2 owl-carousel">
                <% loop ObjectsByTag('Collection', 'show_in_home').shuffle.limit(6) %>
                <div class="single-categories default-overlay">
                    <div class="img-zoom">
                        <a href="$AbsoluteLink"><% include CollectionImage Collection=$Me,W=650,H=650 %></a>
                    </div>
                    <div class="categories-content categorie-posution-{$Pos}">
                        <% if $CustomField('CollectionHashTag') %><span>$CustomField('CollectionHashTag')</span><% end_if %>
                        <h3><a href="$Link">$Title</a></h3>
                        <div class="categories-btn">
                            <a href="$AbsoluteLink">Shop now</a>
                        </div>
                    </div>
                </div>
                <% end_loop %>
            </div>
        </div>
    </div>
<% end_if %>

<% if ObjectsByTag('Product', 'show_in_home').count > 3 %>
<div class="product-area section-padding-1 pb-125">
    <div class="container-fluid">
        <div class="section-title-2 mb-70 text-center">
            <h2>$Theme.CustomField('HomeProductsTitle')</h2>
            <p>$Theme.CustomField('HomeProductsSubTitle')</p>
        </div>

        <div class="row">
            <% loop ObjectsByTag('Product', 'show_in_home').shuffle.limit(12) %>
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6"><% include ProductCard Product=$Me,CSS="mb-60" %></div>
            <% end_loop %>
        </div>
        <div class="btn-style-1 text-center">
            <a class="btn-border" href="/search">
                <div class="btn-viewmore-normal btn-ptb-1 btn-viewmore-bg-transparent btn-viewmore-common">
                    <span>View all products</span>
                </div>
                <div class="btn-viewmore-hover btn-ptb-1 btn-viewmore-common btn-hover-transition">
                    <span>View all products</span>
                </div>
            </a>
        </div>
    </div>
</div>
<% end_if %>

<% if $Theme.CustomField('HomeParallaxImage').Value() %>
    <div class="delay-area res-white-overly-xs">
        <img src="$Theme.CustomField('HomeParallaxImage').Value().Fill(1920,800).AbsoluteLink" class="w-100" />
    </div>
<% end_if %>

<% if ObjectsByTag('Product', 'on_sale').count %>
    <div class="product-area section-padding-1 pt-125 pb-125">
        <div class="container-fluid">
            <div class="section-title-2 mb-70 text-center">
                <h2>$Theme.CustomField('HomeSaleTitle')</h2>
                <p>$Theme.CustomField('HomeSaleSubTitle')</p>
            </div>
            <div class="product-slider-active-2 owl-carousel dot-style-2">
                <% loop ObjectsByTag('Product', 'on_sale').shuffle.limit(6) %>
                <% include ProductCard Product=$Me %>
                <% end_loop %>
            </div>
        </div>
    </div>
<% end_if %>

<% if $Theme.CustomField('HomeTextSection').Value() %>
    <div class="service-area bg-light-pink pt-60 pb-25">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    $Theme.CustomField('HomeTextSection').Value()
                </div>
            </div>
        </div>
    </div>
<% end_if %>