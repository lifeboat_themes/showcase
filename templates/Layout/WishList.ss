<% include PageHeader %>
<% include HeaderAssists %>
<div class="breadcrumb-area bg-gray breadcrumb-ptb-1">
    <div class="container-fluid">
        <div class="breadcrumb-content text-center">
            <div class="breadcrumb-title">
                <h2>Wish list</h2>
            </div>
            <ul>
                <li>
                    <a href="/">Home</a>
                </li>
                <li><span> > </span></li>
                <li class="active">Wish list</li>
            </ul>
        </div>
    </div>
</div>
<div class="shop-area section-padding-1 padding-60-row-col pb-40">
    <div class="container-fluid">
        <div class="row">
            <% if $WishList.Products.count %>
            <% loop $WishList.Products %>
                <div class="col-lg-3 col-md-4">
                    <% include ProductCard Product=$Me,CSS="mb-60" %>
                </div>
            <% end_loop %>
            <% else %>
                <div class="col">
                    <p>You have no products in your wish list.</p>
                </div>
            <% end_if %>
        </div>
    </div>
</div>