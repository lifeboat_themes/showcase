<% include PageHeader %>
<% include HeaderAssists %>
<div class="shop-area section-padding-1 padding-60-row-col pt-100 pb-40">
    <div class="container-fluid">
        <div class="row">
            <% loop $AllCollections %>
            <div class="col-lg-4 col-md-6">
                <div class="shop-collection-wrap default-overlay mb-30">
                    <div class="collection-img">
                        <a href="$AbsoluteLink"><% include CollectionImage Collection=$Me,W=650,H=650 %></a>
                    </div>
                    <div class="shop-collection-content w-100">
                        <h4><a href="$AbsoluteLink">$Title</a></h4>
                    </div>
                </div>
            </div>
            <% end_loop %>
        </div>
    </div>
</div>