<% include PageHeader %>
<% include HeaderAssists %>
<div class="shop-area section-padding-3 pb-100">
    <div class="container-fluid">
        <div class="row flex-row-reverse">
            <div class="col-lg-9">
                <div class="shhop-pl-35">
                    <div class="shop-top-bar">
                        <div class="shop-top-bar-right">
                            <div class="shop-tab nav">
                                <a href="#shop-1" class="active" data-toggle="tab"><i class="dlicon ui-2_grid-45"></i></a>
                                <a href="#shop-2" data-toggle="tab"><i class="dlicon design_bullet-list-69"></i></a>
                            </div>
                            <div class="shop-short-by ml-50">
                                <span>Sort by <i class="fa fa-angle-down angle-down"></i> <i class="fa fa-angle-up angle-up"></i></span>
                                <ul>
                                    <% loop $getOptions().Sort %>
                                        <li <% if $Selected %>class="active"<% end_if %>>
                                            <a href="$FilterLink">$Option</a>
                                        </li>
                                    <% end_loop %>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content jump-3 pt-30">
                        <div id="shop-1" class="tab-pane active">
                            <div class="row">
                                <% loop $PaginatedResults(9) %>
                                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                        <% include ProductCard Product=$Me,CSS="mb-50" %>
                                    </div>
                                <% end_loop %>
                            </div>
                        </div>
                        <div id="shop-2" class="tab-pane">
                            <% loop $PaginatedResults(9) %>
                                <% include HorizontalProductCard Product=$Me %>
                            <% end_loop %>
                        </div>
                        <% if PaginatedResults(9).MoreThanOnePage %>
                            <% with PaginatedResults(9) %>
                            <div class="pro-pagination-style text-center">
                                <ul>
                                    <% if $NotFirstPage %>
                                        <li><a href="$PrevLink"><i class="dlicon arrows-1_tail-left"></i></a></li>
                                    <% end_if %>
                                    <% loop $PaginationSummary %>
                                        <% if $CurrentBool %>
                                            <li><a class="active" href="#">$PageNum</a></li>
                                        <% else %>
                                            <% if $Link %>
                                                <li><a href="$Link">$PageNum</a></li>
                                            <% else %>
                                                <li>...</li>
                                            <% end_if %>
                                        <% end_if %>
                                    <% end_loop %>
                                    <% if $NotLastPage %>
                                        <li><a href="$NextLink"><i class="dlicon arrows-1_tail-right"></i></a></li>
                                    <% end_if %>
                                </ul>
                            </div>
                            <% end_with %>
                        <% end_if %>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="shop-sidebar-style mt-25">
                    <form method="GET" role="form">
                        <div class="sidebar-widget mb-70">
                            <h4 class="pro-sidebar-title">Search</h4>
                            <div class="price-filter mb-70">
                                <div class="price-slider-amount">
                                    <div class="label-input">
                                        <span>Search: </span><input type="text" name="search" placeholder="Search by Name, SKU,..." value="$getSearchTerm">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar-widget mb-90">
                            <h4 class="pro-sidebar-title">Filter by price </h4>
                            <div class="price-filter mt-30">
                                <div class="slider" data-min="$getOptions().PriceRange.Min" data-max="$getOptions().PriceRange.Max">
                                    <div class="ranges pt-3">
                                        <div class="min">$SiteSettings.CurrencySymbol()<span data-role="min">$getOptions().PriceRange.Min</span></div>
                                        <div class="max">$SiteSettings.CurrencySymbol()<span data-role="max">$getOptions().PriceRange.Max</span></div>
                                    </div>
                                    <input type="hidden" name="price_min" data-role="min_val" value="$getOptions().PriceRange.Min" />
                                    <input type="hidden" name="price_max" data-role="max_val" value="$getOptions().PriceRange.Max" />
                                </div>
                            </div>
                        </div>
                        <% loop $getOptions().SearchFilters %>
                            <div class="sidebar-widget mb-30">
                                <h4 class="pro-sidebar-title">$Name</h4>
                                <div class="sidebar-widget-size mt-15">
                                    <% if $Type == 'color' %>
                                        <% loop $SearchData %>
                                            <div class="form-check form-check-inline color-swatch ml-1 mr-1">
                                                <label class="form-check-label" for="$ID">
                                                    <input type="checkbox" name="search_data[]" value="$ID"
                                                           id="$ID" <% if $Selected %>checked<% end_if %> />
                                                    <span class="swatch" style="background: #{$Value}"></span>
                                                </label>
                                            </div>
                                        <% end_loop %>
                                    <% else %>
                                        <% loop $SearchData %>
                                            <div class="form-check form-check-inline text-swatch ml-1 mr-1">
                                                <label class="form-check-label" for="$ID">
                                                    <input type="checkbox" name="search_data[]" value="$ID"
                                                           id="$ID" <% if $Selected %>checked<% end_if %> />
                                                    <span class="swatch">$Label</span>
                                                </label>
                                            </div>
                                        <% end_loop %>
                                    <% end_if %>
                                </div>
                            </div>
                        <% end_loop %>
                        <div class="sidebar-widget">
                            <div class="price-filter">
                                <div class="price-slider-amount">
                                    <button type="submit">Filter</button>
                                </div>
                                <div class="price-slider-amount">
                                    <a href="$getOptions.ResetLink()">Reset</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>