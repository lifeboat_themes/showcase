<% if $Product %>
    <% if $Product.Image.exists %>
        <img src="$Product.Image.Pad($W,$H).AbsoluteLink" alt="$Product.Title" class="$CSS">
    <% else %>
        <img src="$SiteSettings.Logo.Pad($W,$H,FFFFFF).AbsoluteLink" alt="No Image found for $Product.Title" class="$CSS"/>
    <% end_if %>
<% else %>
    <% if $Image.exists %>
        <img src="$Image.Pad($W,$H).AbsoluteLink" alt="$Title" class="$CSS">
    <% else %>
        <img src="$SiteSettings.Logo.Pad($W,$H,FFFFFF).AbsoluteLink" alt="No Image found for $Title" class="$CSS"/>
    <% end_if %>
<% end_if %>