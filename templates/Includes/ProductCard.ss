<div class="product-wrap $CSS">
    <div class="product-img default-overlay mb-25">
        <a href="$Product.AbsoluteLink">
            <% if $Product.Image %>
                <img class="default-img" src="$Product.Image.Pad(600,750,FFFFFF).AbsoluteLink" alt="$Product.Title" />
            <% else %>
                <img src="$SiteSettings.Logo.Pad(600,750,FFFFFF).AbsoluteLink" alt="No Image found for $Product.Title" class="default-img" />
            <% end_if %>

            <% if $Product.ProductImages.count > 1 %>
                <img class="hover-img" src="$Product.ProductImages.limit(2,1).first.Pad(600,750,FFFFFF).AbsoluteLink" alt="$Product.Title" />
            <% else %>
                <% if $Product.Image %>
                    <img class="hover-img" src="$Product.Image.Pad(600,750,FFFFFF).AbsoluteLink" alt="$Product.Title" />
                <% else %>
                    <img src="$SiteSettings.Logo.Pad(600,750,FFFFFF).AbsoluteLink" alt="No Image found for $Product.Title" class="hover-img" />
                <% end_if %>
            <% end_if %>

            <% if $Product.hasDiscount %>
                <span class="badge-red badge-left-20 badge-top-20 badge-width-height-2 badge-border-radius-100">On Sale!</span>
            <% end_if %>

            <% if not $Product.hasStockLeft %>
                <span class="badge-black badge-right-0 badge-top-0 badge-width-height-1">Out of Stock</span>
            <% end_if %>
        </a>
        <div class="product-action product-action-position-2 action-inc-width-height">
            <% include WishListButton Product=$Me %>
            <a title="Shop Now" href="$AbsoluteLink"><i class="fa fa-shopping-cart"></i><span>Shop Now</span></a>
        </div>
    </div>
    <div class="product-content-2 title-font-width-400 text-center">
        <h3><a href="$Product.ShowcaseProduct.AbsoluteLink">$Product.ShowcaseProduct.Title</a></h3>
        <div class="product-price">
            <% if $Product.MinPrice != $Product.MaxPrice %>
                <span class="new-price">$Product.FormatPrice('MinPrice') - $Product.FormatPrice('MaxPrice')</span>
            <% else %>
                <% if $Product.ShowcaseProduct.isDiscounted %>
                    <span class="old-price">$Product.ShowcaseProduct.BasePrice</span>
                <% end_if %>
                <span class="new-price">$Product.ShowcaseProduct.SellingPrice</span>
            <% end_if %>
        </div>
    </div>
</div>