const ProductForm = function (form, container) {
    "use strict";

    let _this = this;

    this.make_select_field = function (option, opts) {
        let elem = $("<select>", {'class': "form-control", 'id': option, 'name': option});

        opts.forEach(function (item) {
            elem.append($("<option>", {'value': item}).html(item));
        });

        return elem;
    };

    this.make_select = function (option, name, options) {
        let select_options = [];

        options.forEach(function (item) {
            select_options.push($("<option>", {'value': item}).html(item));
        });

        return $("<div>", {'class': "mt-2"}).html([
            $("<label>", {'for': option}).html(name + ":"),
            _this.make_select_field(option, options)
        ])
    };

    this.render_price = function () {
        let c = container.find(".price-container").html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
        let btn = form.find('.add-to-cart').addClass('disabled').prop('disabled', true);

        Lifeboat.Product.CalculatePrice(form.data("id"), _this.get_variant_data(), 1, function (data) {
            if (data.StockAmount === 0) {
                c.html("<strong class='text-danger'>Out of Stock</strong>");
            } else {
                let sell_price = "<h3>" + data.SellingPrice + "</h3>";
                if (data.isDiscounted) {
                    c.html("<del class='text-danger'>" + data.BasePrice + "</del>" + sell_price);
                } else {
                    c.html(sell_price);
                }

                btn.removeClass('disabled').prop('disabled', false);
            }
        });
    };

    this.render_variants = function () {
        let v = $("<div>", {'class': "variant-container"});

        v.html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
        Lifeboat.Product.Variants(form.data("id"), function (data) {
            v.html("");

            if (typeof data === 'object' && data !== null) {
                for (let x = 1; x < 4; x++) {
                    let opt_name = 'Option' + x;
                    if (data[opt_name]) {
                        v.append(_this.make_select('Option' + x, data[opt_name].Name, Object.keys(data[opt_name].Options)));
                    }

                    v.find("select#" + opt_name).change(function () {
                        let $this = $(this),
                            elem_id = $this.attr('id'),

                            val = $this.val();

                        for (let y = 1; y < 4; y++) {
                            let check_opt = 'Option' + y;
                            if (elem_id === check_opt) continue;

                            let select  = v.find("select#" + check_opt);
                            let allow   = false;
                            let curr    = select.val();

                            data[elem_id].Options[val][check_opt].forEach(function (item) {
                                if (item === curr) allow = true;
                            });

                            if (!allow) {
                                select.val(data[elem_id].Options[val][check_opt][0]);
                            }
                        }
                        _this.render_price();
                    });
                }

                $(v.find("select")[0]).trigger("change");
            }

            _this.render_price();
        });

        return v;
    };

    this.get_variant_data = function () {
        return {
            "Option1": form.find(".form-control#Option1").val(),
            "Option2": form.find(".form-control#Option2").val(),
            "Option3": form.find(".form-control#Option3").val(),
        };
    };

    this.init = function () {
        container.append($("<div>", {class: "price"}).append([
            $("<div>", {class: "price-container"}),
            _this.render_variants(),
        ]));

        form.find(".add-to-cart").click(function (e) {
            console.log(e);
            e.preventDefault();
            Lifeboat.Cart.AddItem(
                form.data("id"),
                _this.get_variant_data(),
                form.find("#Quantity").val(),
                function () {
                    location.reload();
                }
            );
        });
    }
};

(function($) {
    "use strict";

    $("*[data-role='product']").each(function () {
        let elem = $(this);

        elem.find("*[data-role='quantity']").change(function () {
            Lifeboat.Cart.UpdateQuantity(elem.data('id'), elem.data('variant'), $(this).val(), function () { location.reload(); });
        });

        elem.find("*[data-role='delete']").click(function (e) {
            e.preventDefault();
            Lifeboat.Cart.RemoveItem(elem.data('id'), elem.data('variant'), function () { location.reload(); });
        });
    });

    $("*[data-role='product-form']").each(function () {
        let pf = new ProductForm($(this), $($(this).data('form')));
        pf.init();
    });

    $(".wishlist-toggle").click(function (e) {
        e.preventDefault();
        Lifeboat.WishList.Toggle($(this).data("id"), function () {
            location.reload();
        })
    });

    $('.slider').each(function(e) {

        let slider = $(this),
            min_display = slider.find("*[data-role='min']"),
            max_display = slider.find("*[data-role='max']"),
            min_val = slider.find("*[data-role='min_val']"),
            max_val = slider.find("*[data-role='max_val']"),
            min = parseInt(slider.data("min")),
            max = parseInt(slider.data("max")),
            step = (max - min) / 100;

        slider.slider({
            'range': true,
            'values': [min, max],
            'min': min,
            'step': step,
            'minRange': step,
            'max': max,
            'slide': function( event, ui ) {
                min_display.html(ui.values[0]);
                max_display.html(ui.values[1]);

                min_val.val(ui.values[0]);
                max_val.val(ui.values[1]);
            }
        });
    });

})(jQuery);

(function(){
    let frame = document.getElementById('map_frame_container');
    let key = document.getElementById('map_frame_key');
    let search = document.getElementById('map_frame_query');
    if (!frame || !key || !search) return this;

    let map = document.createElement('iframe');
    map.src = 'https://www.google.com/maps/embed/v1/place?q=' + search.value.replace(' ', '+') + '&key=' + key.value;
    map.style.border = "0";
    map.style.height = "100%";
    map.style.width = "100%";
    map.setAttribute('allow', 'fullscreen');
    map.setAttribute('loading', 'lazy');

    frame.appendChild(map);
})(jQuery);
