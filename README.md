# Showcase
A popular retail theme, mainly focused towards retail of clothing and cosmetics.

## Generic
Fields and options that used throughout your Lifeboat store, when using this theme.

### Footer: Quick Links Menu
You are given the option to select one of your menus to make up the quick links
section in the footer.<br />
Quick Links could include: Terms of Use, Privacy Policy, Return Policy...<br />
_Note: Submenus items will not be displayed_<br />
_Recommendation: For the best results do not add more then 6 items in this section_

### Footer: Company Links Menu
Similar to the Quick Links Section, you may select a menu to make up this section.<br />
Company Links could include: About Us, Contact Page...<br />
_Note: Submenus items will not be displayed_<br />
_Recommendation: For the best results do not add more then 6 items in this section_

## Home Page
Fields and options that make up the home page when using this theme.

### Banner
#### Adding Banners
You will find 3 drop downs in the theme customisation options
that will allow you to select 3 products to feature in the banner section.

#### Custom Fields
Home: Featured Product 1: _The first product that will be featured in the banner_<br />
Home: Featured Product 2: _The second product that will be featured in the banner_<br />
Home: Featured Product 3: _The third product that will be featured in the banner_<br />
Product > Home Banner Image:
If you fill in this field, this image will be used in the banner.<br />
_Note: If left blank, the first image from the product will be used._<br />
_Note: Recommended Size: 900px x 1200px_

### Categories Section
#### Adding Collections
To add collections in home page, add the tag: **show_in_home**,
to the collection you want to list.<br />
_Note: The order of the collections will be randomised on every 
page load and only 6 collections can be shown._

#### Custom Fields
Home: Categories Section Title: _Title of the Categories section._<br />
Home: Categories Section Subtitle: _A subtitle added underneath the Categories section title._<br />
Collection > HashTag: _To add a hash tag label to the Collection's card._

### Product Gallery Section
#### Adding Products
To add products to the Gallery Section in the home page, add
the tag **show_in_home**, to the products you want to add.<br />
_Note: You need at least 4 products with the tag show_in_home for the section to be visible_<br />
_Note: Maximum 12 items will be shown_<br />
_Note: Order of products is randomised with every page load_

#### Custom Fields
Home: Products Section Title: _Title of the Products Gallery Section_<br />
Home: Products Section Subtitle: _A subtitle added underneath the Products section title._

### Parallax Image
This image is intended to break up a little bit the home page from constant
product listings. It's recommended to add an image of a running promotion,
testimonial, or other relevant information.

#### Custom Fields
Home: Parallax Image: _The image to be used as the Parallax Image_ 

### Sale Section
#### Adding Products
To add products to the Sale section in the home page, add
the tag **on_sale**, to the products you want to add.<br />
_Note: Maximum of 6 products will be shown_
_Note: Order of products is randomised with every page load_

#### Custom Fields
Home: Sale Section Title: _Title of the Sale Section_<br />
Home: Sale Section Subtitle: _A subtitle added underneath the Sale section title._

### Bottom Section
This section is a free text / html section which you could you use to point out
important information about your store.

#### Custom Fields
Home: Bottom Section: _Contents for the Bottom Section_

